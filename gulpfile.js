// Path
const project_folder = "dist";
const source_folder = "src";

const path = {
    build: {
        html: project_folder + "/",
        css: project_folder + "/css",
        js: project_folder + "/js",
        img: project_folder + "/img",
        fonts: project_folder + "/fonts/",
    },
    src: {
        html: "*.html",
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/script.js",
        img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
        fonts: source_folder + "/fonts/*ttf",
    },
    watch: {
        html: "*.html",
        css: source_folder + "/scss/**/*.scss",
        js: source_folder + "/js/**/*.js",
        img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    },
    clean: "./" + project_folder + "/"
}

const { notify } = require("browser-sync");
const gulp = require("gulp");
const {src, dest} = require("gulp");
const fileInclude = require('gulp-file-include');
const clean = require("gulp-clean");
const del = require("del");
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require("gulp-autoprefixer");
const cleancss = require("gulp-clean-css");
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const jsmin = require("gulp-jsmin");
const imagemin = require("gulp-imagemin");
const browserSync = require("browser-sync").create();

// Tasks

function sync() {
    browserSync.init({
        server: {
           baseDir: "./" + project_folder + "/"
        },
        port: 3010,
        notify: false,
    })
}

function html() {
    return src(path.src.html)
      .pipe(fileInclude())
      .pipe(dest(path.build.html))
      .pipe(browserSync.stream())
}

function css() {
    return src(path.src.css)
      .pipe(
        sass({
            outputStyle: "expanded",
        }).on('error', sass.logError)
      )
      .pipe(autoprefixer({
        "overrideBrowserslist":'last 2 version',
        cascade: true
       }))
      .pipe(dest(path.build.css))
      .pipe(cleancss())
      .pipe(
        rename({
            extname: ".min.css"
        })
      )
      .pipe(dest(path.build.css))
      .pipe(browserSync.stream())
}

function js() {
    return src(path.src.js)
      .pipe(fileInclude())
      .pipe(dest(path.build.js))
      .pipe(jsmin())
      .pipe(
        rename({
            extname: ".min.js"
        })
      )
      .pipe(dest(path.build.js))
      .pipe(browserSync.stream())
}

function images() {
    return src(path.src.img)
       .pipe(
        imagemin ({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true,
            optimizationLevel: 2
        })
       )
      .pipe(dest(path.build.img))
      .pipe(browserSync.stream())
}

function watchFiles() {
    gulp.watch([path.watch.html], html);
    gulp.watch([path.watch.css], css).on('change', browserSync.reload);
    gulp.watch([path.watch.js], js).on('change', browserSync.reload);
    gulp.watch([path.watch.img], images);
}

function cleanFolder() {
    return gulp.src(path.clean)
        .pipe(clean({force:true}));
}


// Build

const build = gulp.series(cleanFolder, html, gulp.parallel(css, js, images));
const dev = gulp.series(build, gulp.parallel(build, watchFiles, sync))

exports.dev = dev;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.default= dev;
